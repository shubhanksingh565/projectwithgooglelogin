<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Excel Import in CI</title>
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/bootstrap.min.css'; ?>">
    <style>
        body {
            background-color: beige;
        }
    </style>
</head>

<body>
    <div class="navbar navbar-dark bg-dark">
        <div class="container">
            <a href="#" class="navbar-brand">Excel Importing</a>
        </div>
    </div>
    <div>

        <div class="container">
            <div class="row" style="margin-top: 20px;">
                <form action="<?php echo base_url(); ?>Excel_import/importFile" method="post" enctype="multipart/form-data">
                    <p><label><b>Select Excel File</b></label></p>
                    <input type="file" name="uploadFile" value="" required accept=".xls, .xlsx, .csv">
                    <button type="submit" class="btn btn-info" name="Import" value="Import">Upload</button>
                </form>



                <div class="table-responsive" id="student_data" style="margin-top: 20px;">
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <tr style="font-weight: bolder;">
                                <td>Sr. No.</td>
                                <td>Student Name</td>
                                <td>Class</td>
                                <td>Phone No.</td>
                            </tr>
                            <?php
                            if (!empty($getData)) {
                                foreach ($getData as $value) { ?>
                                    <tr>
                                        <td width="0"><?php echo $value['id'] ?></td>
                                        <td width=""><?php echo $value['stud_name'] ?></td>
                                        <td width=""><?php echo $value['stud_class'] ?></td>
                                        <td width=""><?php echo $value['stud_phone'] ?></td>
                                    </tr>
                                <?php }
                            } else { ?>

                                <tr>
                                    <td colspan="4">Records not found </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>