<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crud APP - Add Detail</title>
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/bootstrap.min.css'; ?>">
    <style>
        /* div {
            border: 1px solid red;
        } */

        body {
            background-color: beige;
        }
    </style>
</head>


<body>
    <div class="navbar navbar-dark bg-dark">
        <div class="container">
            <a href="#" class="navbar-brand">CRUD OPERATION </a>
        </div>
    </div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-6">
                        <h3>Fill your Details</h3>
                    </div>
                    <div class="col-6 text-right">
                        <a href="<?php echo base_url() . 'user/list'; ?>" class="btn btn-warning">Back to Manage Details</a>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <?php
        if (isset($user_id))
            $create_update = 'update';
        else
            $create_update = 'create';

        ?>
        <form name="myForm" method="post" action="<?php echo base_url() . 'user/' . $create_update; ?> ">
            <div class="row">
                <div class="form-group">
                    <div class="col-md-6">
                        <label>Full Name :</label>
                        <input type="text" class="form-control" name="name" value="<?php if (isset($name)) echo $name; ?>">
                        <?php echo form_error('name'); ?>
                    </div>
                </div>
                <br>
                <br>
                <br>

                <div class="form-group">
                    <div class="col-md-6">
                        <label>Phone No. :</label>
                        <input type="text" class="form-control" name="phone"  pattern="[0-9]{10}" value="<?php if (isset($phone)) echo $phone; ?>">
                        <?php echo form_error('phone'); ?>
                    </div>
                </div>
                <br>
                <br>
                <br>
                <!-- pattern="[0-9]{10}" -->

                <!-- <div class="form-group">
                    <div class="col-md-6">
                        <label>Phone No. :</label>
                        <input type="text" class="form-control" name="phone" pattern="[0-9]{10}"
                        value="<?php //if (isset($phone)) echo $phone; ?>">

                        <?php //echo form_error('phone'); ?>
                    </div>
                </div>
                <br>
                <br>
                <br> -->

                <input type="hidden" value=<?php if (isset($user_id)) echo $user_id; ?> name="user_id">

                <div class="form-group">
                    <div class="col-md-6">
                        <label>Gender :</label>
                        <input type="radio" id="male" name="gender" value="male" <?php if(isset($gender)) echo ($gender== "male" ? 'checked="checked"': '');?>style="margin-left:30px">
                        <label for="male">Male</label>
                        <input type="radio" id="female" name="gender" value="female" <?php if(isset($gender)) echo ($gender== "female" ? 'checked="checked"': '');?>style="margin-left:60px">
                        <label for="female">Female</label><br>
                        <?php echo form_error('gender'); ?>
                    </div>
                </div>
                <br>
                <br>


                <div class="form-group">
                    <div class="col-md-6">
                        <label>Address :</label>
                        <textarea class="form-control" name="address"><?php if(isset($address))echo $address;?></textarea>
                        <?php echo form_error('address');?>
                    </div>
                </div>
                <br>
                <br>
                <br>
                <br>

                <div class="form-group">
                    <div class="col-md-6">
                        <label>Select Course</label>
                        <select class="form-select" name="course">
                            <option value="" selected>Select Course</option>
                            <option value="BCA" <?php if(isset($course)){if($course == 'BCA') echo 'selected';}?>>BCA</option>
                            <option value="B.tech" <?php if(isset($course)){if($course == 'B.tech') echo 'selected';}?>>B.tech</option>
                            <option value="M.tech"<?php if(isset($course)){ if($course == 'M.tech') echo 'selected';}?>>M.tech</option>
                        </select>
                        <?php echo form_error('course'); ?>
                    </div>
                </div>
                <br>
                <br>
                <br>
                <br>
                <div class="form-group">
                    <div class="col-md-6">
                        &nbsp; <input type="checkbox" id="checkbox" name="checkbox" value="">
                        &nbsp;
                        <label for="checkbox">I have read and agree to the Terms and Conditions and Privacy Policy</label>
                        <?php echo form_error('checkbox'); ?>
                    </div>
                </div>
                <br>
                <br>
               
                <div class="form-group">

                    <?php if (isset($user_id)) { ?>
                        <button class="btn btn-success">Update</button>
                    <?php } else { ?>
                        <button class="btn btn-success">Submit</button>
                    <?php } ?>

                    <a href="" class="btn btn-danger"> Clear</a>
                </div>
            </div>
        </form>
    </div>
</body>

</html>