<?php
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(E_ALL);
ini_set('display_errors', 0);
class Excel_import extends CI_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Excel_import_model', 'modelObj');
        $this->load->library('Excel');
    }

    public function Excel_import_view()
    {
        $getData = $this->modelObj->getStudent_data();
        $data = array();
        $data['getData'] = $getData;
        $this->load->view('excel_import_view', $data);
    }

    public function importFile()
    {

        if ($this->input->post('Import')) {
            // echo "working";
            // exit;
            $path = 'uploads/';
            require_once APPPATH . "/third_party/PHPExcel.php";
            $config['upload_path'] = $path;
            $config['allowed_types'] = 'xlsx|xls|csv';
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $data = array('upload_data' => $this->upload->data());

            echo "<pre>";
            print_r($data);
            exit;
            
            if ($data) {
                if (!empty($data['upload_data']['file_name'])) {
                    $import_xls_file = $data['upload_data']['file_name'];
                } else {
                    $import_xls_file = 0;
                }
                $inputFileName = $path . $import_xls_file;
                try {
                    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFileName);
                    $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                    $flag = true;
                    $i = 0;
                    foreach ($allDataInSheet as $value) {
                        if ($flag) {
                            $flag = false;
                            continue;
                        }
                        $inserdata[$i]['id'] = $value['A'];
                        $inserdata[$i]['stud_name'] = $value['B'];
                        $inserdata[$i]['stud_class'] = $value['C'];
                        $inserdata[$i]['stud_phone'] = $value['D'];
                        $i++;
                    }
                    $result = $this->import->insert($inserdata);
                    if ($result) {
                        echo "Imported successfully";
                    } else {
                        echo "ERROR !";
                    }
                } catch (Exception $e) {
                    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                        . '": ' . $e->getMessage());
                }
            } else {
                echo $error['error'];
            }
        } else {
            echo "not working";
        }
        // redirect(base_url() . 'Excel_import');
        // $this->load->view('excel_import_view');
    }
}

// echo "<pre>";
// print_r($data);
// exit;