<html>

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <title>Login with Google in Codeigniter</title>
   <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport' />
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
   <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
   <style>
      /* @import url('https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&display=swap'); */

      .panel {
         text-align: center;
      }

      .icon {
         width: 100px;
         height: 100px;
         position: relative;
      }

      .status-circle {
         width: 21px;
         height: 21px;
         border-radius: 50%;
         background-color: green;
         border: 2px solid white;
         bottom: 2px;
         right: 10px;
         position: absolute;
      }
   </style>
</head>

<body>
   <div class="container">
      <br />
      <?php if (empty($this->session->userdata('user_data'))) { ?>
         <div class="panel panel-default">
            <div>
               <img src="https://assets.materialup.com/uploads/0fd8e590-3432-49e9-aa75-1a3acc6a0e15/Gplus-2x.gif" width="400" height="250">
            </div>

            <h2>Sign In</h2>
            <p>Use your Google Account</p>
         <?php } ?>
         <!-- <br /> -->
         <!-- </div> -->

         <?php
         if (!isset($login_button)) {
            $user_data = $this->session->userdata('user_data');
            echo '<div class="panel-heading">Welcome&nbsp;' . $user_data["first_name"] . '</div><div class="panel-body">';
            echo '<div class="icon"> <img src="' . $user_data['profile_picture'] . '" class="img-responsive img-circle img-thumbnail" />';
            echo ' <div class="status-circle"></div></div>';
            echo '<h3><b>Your Name : </b>' . $user_data["first_name"] . ' ' . $user_data['last_name'] . '</h3>';
            echo '<h3><b>Your Email :</b> ' . $user_data['email_address'] . '</h3>';
            echo '<h3><a href="' . base_url() . 'google_logout">Logout</h3></div>';
         } else {
            echo '<div align="center" style="margin-bottom:20px;  ">' . $login_button . '</div>';
         }
         ?>
         </div>
   </div>
</body>

</html>