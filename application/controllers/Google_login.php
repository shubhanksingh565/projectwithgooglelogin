
   <?php
   defined('BASEPATH') or exit('No direct script access allowed');
   // ini_set('display_errors', 1);
   // ini_set('display_startup_errors', 1);
   // error_reporting(E_ALL);
   class Google_login extends CI_Controller
   {

      public function __construct()
      {
         parent::__construct();
         $this->load->model('google_login_model');
         $this->load->helper('google_login_helper');
      }

      public function login()
      {
         $google_client = google_check_auth();
         $login_button = '';
         if (!$this->session->userdata('access_token')) {
            $login_button = '<a href="' . $google_client->createAuthUrl() . '"><img src="' . base_url() . 'assets/sign-in-with-google.png" /></a>';
            $data['login_button'] = $login_button;
            $this->load->view('google_login', $data);
         } else {
            $this->load->view('google_login');
         }
      }

      function checklogin()
      {
         $google_client = google_check_auth();
         if (isset($_GET["code"])) {
            $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);
            if (!isset($token["error"])) {

               $google_client->setAccessToken($token['access_token']);
               $this->session->set_userdata('access_token', $token['access_token']);
               $google_service = new Google\Service\Oauth2($google_client);
               $data = $google_service->userinfo->get();

               if ($this->google_login_model->Is_already_register($data['id'])) {
                  //update data
                  $user_data = array(
                     'first_name' => $data['given_name'],
                     'last_name'  => $data['family_name'],
                     'email_address' => $data['email'],
                     'profile_picture' => $data['picture'],
                     'updated_at' => date('Y-m-d H:i:s')
                  );

                  $this->google_login_model->Update_user_data($user_data, $data['id']);
               } else {
                  //insert data
                  $user_data = array(
                     'login_oauth_uid' => $data['id'],
                     'first_name'  => $data['given_name'],
                     'last_name'   => $data['family_name'],
                     'email_address'  => $data['email'],
                     'profile_picture' => $data['picture'],
                     'created_at'  => date('Y-m-d H:i:s')
                  );

                  $this->google_login_model->Insert_user_data($user_data);
               }
               $this->session->set_userdata('user_data', $user_data);
               redirect('googlelogin');
            } else {
               redirect('googlelogin');
            }
         }
      }

      function logout()
      {
         $this->session->unset_userdata('access_token');
         $this->session->unset_userdata('user_data');
         redirect('google_login/login');
      }
   }
