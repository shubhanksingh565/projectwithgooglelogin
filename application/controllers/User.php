<?php

class User extends CI_controller
{

   // public function list()  
   // {  
   //    //load the database  
   //    $this->load->database();  
   //    //load the model  
   //    $this->load->model('User_model');  
   //    //load the method of model  
   //    $data['h']=$this->User_model->select();  
   //    //return the data in view  
   //    $this->load->view('list', $data);  
   // }  

   public function create()
   {
      $this->load->model('User_model');
      $this->form_validation->set_rules('name', 'Input', 'required');
      $this->form_validation->set_rules('phone', 'Input', 'required');
      $this->form_validation->set_rules('gender', 'Input', 'required');
      $this->form_validation->set_rules('address', 'Input', 'required');
      $this->form_validation->set_rules('course', 'Select', 'required');
      // $this->form_validation->set_rules('checkbox', 'checkbox', 'required');


      if ($this->form_validation->run() == false) {
         $this->load->view('create');
      } else {
         $formArray = array();
         $formArray['name'] = $this->input->post('name');
         $formArray['phone'] = $this->input->post('phone');
         $formArray['gender'] = $this->input->post('gender');
         $formArray['address'] = $this->input->post('address');
         $formArray['course'] = $this->input->post('course');
         // $formArray['checkbox'] = $this->input->post('checkbox');
         $formArray['created_at'] = date('Y-m-d');
         $this->User_model->create($formArray);
         echo '<script>alert("Value inserted Successfully");</script>';

         $this->session->set_flashdata('success', 'Record added Successfully');
         redirect(base_url() . 'user/list');
      }
   }


   function list()
   {
      $this->load->model('User_model');
      $getData = $this->User_model->getAllRecords('users',array());
      $data = array();
      $data['getData'] = $getData;
      $this->load->view('list', $data);
   }

   function edit()
   {
      $userId = $_GET['user_id'];
      $this->load->model('User_model');
      $user = $this->User_model->getUser($userId);
      $this->load->view('create', $user);
   }

   function delete($userId){
      $this->load->model('User_model');
      $user = $this->User_model->deleteUser($userId);
      if(empty($user)){
         $this->session->set_flashdata('failure', 'Records Deleted Successfully');
         redirect(base_url() . 'user/list');
      }

      $user = $this->User_model->deleteUser($userId);
      $this->session->set_flashdata('failure', 'Records Deleted Successfully');
      redirect(base_url() . 'user/list');




   }

   function update(){
      $userId = $_POST['user_id'];
      $this->load->model('User_model');

      $this->form_validation->set_rules('name', 'Input', 'required');
      $this->form_validation->set_rules('phone', 'Input', 'required');
      $this->form_validation->set_rules('gender', 'Input', 'required');
      $this->form_validation->set_rules('address', 'Input', 'required');
      // $this->form_validation->set_rules('course', 'Select', 'required');
      // $this->form_validation->set_rules('checkbox', 'checkbox', 'required');


      if ($this->form_validation->run() == false) {
         $this->load->view('create', $_POST);
      } else {
         $formArray = array();
         $formArray['name'] = $this->input->post('name');
         $formArray['phone'] = $this->input->post('phone');
         $formArray['gender'] = $this->input->post('gender');
         $formArray['address'] = $this->input->post('address');
         $formArray['course'] = $this->input->post('course');
         // $formArray['checkbox'] = $this->input->post('checkbox');
         $formArray['created_at'] = date('Y-m-d');
   
         $user = $this->User_model->updateUser($userId, $formArray);

         // $this->User_model->create($formArray);
         echo '<script>alert("Data updated Successfully");</script>';

         $this->session->set_flashdata('success', 'Record updated Successfully');
         redirect(base_url() . 'user/list');
      }
   }
}

?>