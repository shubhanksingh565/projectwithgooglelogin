<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crud APP - Manage Details</title>
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/bootstrap.min.css'; ?>">
    <style>
        /* div {
            border: 1px solid red;
        } */

        body {
            background-color: beige;
        }
    </style>
</head>

<body>
    <div class="navbar navbar-dark bg-dark">
        <div class="container">
            <a href="#" class="navbar-brand">CRUD OPERATION </a>
        </div>
    </div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                $success = $this->session->userData('success');
                if ($success != "") {
                ?>
                    <div class="alert alert-success"> <?php echo $success; ?></div>
                <?php
                }
                ?>
                <?php
                $failure = $this->session->userData('failure');
                if ($failure != "") {
                ?>
                    <div class="alert alert-danger"> <?php echo $failure; ?></div>
                <?php
                }
                ?>
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-6">
                        <h3>Manage Details</h3>
                    </div>
                    <div class="col-6 text-right">
                        <a href="<?php echo base_url() . 'user/create'; ?>" class="btn btn-warning">+Add</a>
                    </div>
                </div>
            </div>
        </div>
        <hr>

        <div class="row">
            <div class="col-md-12">

                <div style="font-weight: 500; color:blue;" class="col-md-2">
                    <table class="table table-striped">
                        <tr> <?php $i = count($getData);
                                echo "Total Record&nbsp; " . $i; ?></tr>
                    </table>
                </div>
                <table class="table table-striped">
                    <tr style="font-weight: bolder;">
                        <td>Sr. No.</td>
                        <td>Name</td>
                        <td>Phone No.</td>
                        <td>Gender </td>
                        <td>Address</td>
                        <td>Course</td>
                        <td>Edit</td>
                        <td>Delete</td>
                    </tr>


                    <?php

                    if (!empty($getData)) {
                        foreach ($getData as $value) { ?>

                            <tr>

                                <!-- <td width="70" style="color:red;font-weight: bold;"><?php //echo  $key + 1; ?></td> extra one way to print sr. numbers. -->
                                <td width="70" style="color:red;font-weight: bold;"><?php echo array_search($value, $getData) + 1; ?></td>
                                <td width="0"><?php echo $value['name'] ?></td>
                                <td width=""><?php echo $value['phone'] ?></td>
                                <td width=""><?php echo $value['gender'] ?></td>
                                <td width=""><?php echo $value['address'] ?></td>
                                <td width="120"><?php echo $value['course'] ?></td>

                                <td width="">
                                    <a href="<?php echo base_url() . 'user/edit?user_id=' . $value['user_id'] ?>" class="btn btn-primary">Edit</a>
                                </td>
                                <td width="120">
                                    <a href="<?php echo base_url() . 'user/delete/' . $value['user_id'] ?>" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>

                        <?php }
                    } else { ?>

                        <tr>
                            <td colspan="5">Records not found </td>
                        </tr>
                    <?php } ?>

                </table>

            </div>

        </div>



    </div>
</body>

</html>